# MultiAgents for Pacman

## Overview
This file, `multiAgents.py`, implements various strategies Pacman game. It introduces multiple agents that utilize different approaches learned in chapters 5-6 to decision-making in an adversarial environment.

## Implemented Agents and Functions
### ReflexAgent
- Chooses actions based only on an immediate evaluation of the game state, using a simple heuristic function.

### MinimaxAgent
- Implements the Minimax algorithm to optimize decisions against competent adversarial strategies by other game characters.

### AlphaBetaAgent
- Enhances the MinimaxAgent by integrating alpha-beta pruning, improving efficiency by skipping evaluations of certain branches in the game tree.

### ExpectimaxAgent
- Uses the Expectimax algorithm to handle decisions in scenarios with probabilistic behaviors of other characters, providing a more realistic approach in games like Pacman.

### betterEvaluationFunction
- An advanced evaluation function that provides a more detailed scoring system based on current game state features, such as distances to ghosts and food, incorporating both penalties and rewards.
