# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
from util import manhattanDistance
import random, util

from game import Agent

PACKMAN_AGENT_INDEX = 0
INF = float('inf')
NEGATIVE_INF = float('-inf')


class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """

    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        # scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newGhostStates = successorGameState.getGhostStates()

        ghostDistances = []
        for ghostStates in newGhostStates:
            ghostPosition = ghostStates.configuration.getPosition()
            distanceToGhost = manhattanDistance(ghostPosition, newPos)
            ghostDistances.append(distanceToGhost)
        closestGhost = min(ghostDistances)

        foodDistances = []
        for foodPosition in currentGameState.getFood().asList():
            distanceToGhost = manhattanDistance(foodPosition, newPos)
            foodDistances.append(distanceToGhost)
        closestFood = min(foodDistances)

        return successorGameState.getScore() + closestGhost / (closestFood + 1)


def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()


class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn='scoreEvaluationFunction', depth='2'):
        self.index = 0  # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    def miniMax(self, gameState, action, depth, agentIndex):
        if action not in gameState.getLegalActions(agentIndex):
            return -1

        newGameState = gameState.generateSuccessor(agentIndex, action)
        numAgents = gameState.getNumAgents()
        newAgentIndex = (agentIndex + 1) % numAgents
        newDepth = depth + 1 if newAgentIndex == PACKMAN_AGENT_INDEX else depth
        legalMoves = newGameState.getLegalActions(newAgentIndex)

        if newGameState.isWin() or newGameState.isLose() or not legalMoves or newDepth >= self.depth:
            return self.evaluationFunction(newGameState)

        successorsScores = [self.miniMax(newGameState, newAction, newDepth, newAgentIndex) for newAction in legalMoves]
        if newAgentIndex == PACKMAN_AGENT_INDEX:
            return max(successorsScores)
        else:
            return min(successorsScores)

    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        legalMoves = gameState.getLegalActions()
        scores = [self.miniMax(gameState, action, 0, PACKMAN_AGENT_INDEX) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)
        return legalMoves[chosenIndex]


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def minValue(self, gameState, depth, alpha, beta, agentIndex):
        if gameState.isWin() or gameState.isLose():
            return self.evaluationFunction(gameState)
        bestScore = INF
        for action in gameState.getLegalActions(agentIndex):
            newGameState = gameState.generateSuccessor(agentIndex, action)
            if agentIndex + 1 == gameState.getNumAgents():
                score = self.maxValue(newGameState, depth, alpha, beta)
            else:
                score = self.minValue(newGameState, depth, alpha, beta, agentIndex + 1)
            bestScore = min(score, bestScore)
            if bestScore < alpha:
                return score
            beta = min(beta, bestScore)
        return bestScore

    def maxValue(self, gameState, depth, alpha, beta):
        if depth == 0 or gameState.isWin() or gameState.isLose():
            return self.evaluationFunction(gameState)
        bestScore = NEGATIVE_INF
        for action in gameState.getLegalActions(PACKMAN_AGENT_INDEX):
            newGameState = gameState.generateSuccessor(PACKMAN_AGENT_INDEX, action)
            score = self.minValue(newGameState, depth - 1, alpha, beta, PACKMAN_AGENT_INDEX + 1)
            bestScore = max(bestScore, score)
            if bestScore > beta:
                return score
            alpha = max(alpha, bestScore)
        return bestScore

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        alpha, beta = NEGATIVE_INF, INF
        bestScore = NEGATIVE_INF
        bestAction = None
        for action in gameState.getLegalActions(PACKMAN_AGENT_INDEX):
            newGameState = gameState.generateSuccessor(PACKMAN_AGENT_INDEX, action)
            score = self.minValue(newGameState, self.depth - 1, alpha, beta, PACKMAN_AGENT_INDEX + 1)
            if score > bestScore:
                bestScore = score
                bestAction = action
            alpha = max(alpha, bestScore)
        return bestAction


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def expectimax(self, gameState, depth, agentIndex):
        if depth == 0 or gameState.isWin() or gameState.isLose():
            return self.evaluationFunction(gameState)
        legalMoves = gameState.getLegalActions(agentIndex)
        if not legalMoves:
            return 0
        scores = []
        for action in legalMoves:
            newGameState = gameState.generateSuccessor(agentIndex, action)
            nextAgentIndex = (agentIndex + 1) % gameState.getNumAgents()
            nextDepth = depth - 1 if nextAgentIndex == PACKMAN_AGENT_INDEX else depth
            score = self.expectimax(newGameState, nextDepth, nextAgentIndex)
            scores.append(score)

        if agentIndex == PACKMAN_AGENT_INDEX:
            return max(scores)
        else:
            return sum(scores) / len(scores)

    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        bestScore = NEGATIVE_INF
        bestAction = None
        for action in gameState.getLegalActions(PACKMAN_AGENT_INDEX):
            newGameState = gameState.generateSuccessor(PACKMAN_AGENT_INDEX, action)
            score = self.expectimax(newGameState, self.depth, PACKMAN_AGENT_INDEX + 1)
            if score > bestScore:
                bestScore = score
                bestAction = action
        return bestAction


def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    This function calculate the best score for a given game state.
    The score calculation is based on the following formula:
     - currentGameState score
     - Distance packman and the closest food on board
     - Distance packman and the closest ghost on board:
       * If the ghost is scared (Packman can hunt her), we increase the score based on proximity
       * If the ghost is not scared, and we are too close to the ghost, we add a penalty factor
         so this game state will count as a bad move
    """
    GHOST_TOO_CLOSE_PENALTY = 100  # penalty for close ghosts when they cannot be hunted
    packmanPos = currentGameState.getPacmanPosition()
    foodList = currentGameState.getFood().asList()
    ghostStates = currentGameState.getGhostStates()
    stateScore = currentGameState.getScore()

    foodDistances = [manhattanDistance(packmanPos, food) for food in foodList]
    if foodDistances:
        stateScore += (1 / min(foodDistances))

    for ghost in ghostStates:
        distance = manhattanDistance(packmanPos, ghost.getPosition())
        if ghost.scaredTimer > 0:
            stateScore += ghost.scaredTimer / distance
        else:
            if distance <= 1:
                stateScore -= GHOST_TOO_CLOSE_PENALTY
    return stateScore


# Abbreviation
better = betterEvaluationFunction
